<?php

/**
 * @file
 * Administrative page callbacks for the multispeak module.
 */

/**
 * Implements hook_admin_settings_form().
 */
function multispeak_admin_settings_form($form_state) {

  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
  );

  $form['account']['multispeak_account'] = array(
    '#title' => t('Company ID'),
    '#type' => 'textfield',
    '#default_value' => variable_get('multispeak_account'),
    '#size' => 10,
    '#maxlength' => 10,
    '#required' => TRUE,
    '#description' => t('To get your company ID number, <a target="_blank" href="@register">register your site with Multispeak</a>, or if you already have registered your site, go to your Multispeak <a target="_blank" href="@admin">admin page</a> to get the ID under "Settings > Code".', array('@register' => 'https://multispeak.io/#get_started', '@admin' => url('https://multispeak.io/admin'))),
  );

  // Page specific visibility configurations.
  $visibility = variable_get('multispeak_visibility_pages', 0);
  $pages = variable_get('multispeak_pages', MULTISPEAK_PAGES);

  $form['account']['page_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pages'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  if ($visibility == 2) {
    $form['account']['page_vis_settings'] = array();
    $form['account']['page_vis_settings']['multispeak_visibility_pages'] = array('#type' => 'value', '#value' => 2);
    $form['account']['page_vis_settings']['multispeak_pages'] = array('#type' => 'value', '#value' => $pages);
  }
  else {
    $options = array(
      t('Every page except the listed pages'),
      t('The listed pages only'),
    );
    $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.",
      array(
        '%blog' => 'blog',
        '%blog-wildcard' => 'blog/*',
        '%front' => '<front>',
      )
    );

    $title = t('Pages');
    $form['account']['page_vis_settings']['multispeak_visibility_pages'] = array(
      '#type' => 'radios',
      '#title' => t('Add the widget to specific pages'),
      '#options' => $options,
      '#default_value' => $visibility,
    );
    $form['account']['page_vis_settings']['multispeak_pages'] = array(
      '#type' => 'textarea',
      '#title' => $title,
      '#title_display' => 'invisible',
      '#default_value' => $pages,
      '#description' => $description,
      '#rows' => 10,
    );
  }

  // Render the role overview.
  $form['account']['role_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Roles'),
  );

  $form['account']['role_vis_settings']['multispeak_visibility_roles'] = array(
    '#type' => 'radios',
    '#title' => t('Add the widget for specific roles'),
    '#options' => array(
      t('Add to the selected roles only'),
      t('Add to every role except the selected ones'),
    ),
    '#default_value' => variable_get('multispeak_visibility_roles', 1),
  );

  $role_options = array_map('check_plain', user_roles());
  $form['account']['role_vis_settings']['multispeak_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Roles'),
    '#default_value' => variable_get('multispeak_roles', array()),
    '#options' => $role_options,
    '#description' => t('If none of the roles are selected, all users will see the widget. If a user has any of the roles checked, that user will be have (or not) the widget (depending on the setting above).'),
  );

  return system_settings_form($form);
}

/**
 * Implements _form_validate().
 */
function multispeak_admin_settings_form_validate($form, &$form_state) {

  if (drupal_strlen($form_state['values']['multispeak_account']) != 10) {
    form_set_error('multispeak_account', t('Please enter a valid Multispeak company id.'));
  }
}

/**
 * Layout for the custom variables table in the admin settings form.
 */
function theme_multispeak_admin_custom_var_table($variables) {

  $form = $variables['form'];

  $header = array(
    array('data' => t('Index')),
    array('data' => t('Value')),
  );

  $rows = array();
  foreach (element_children($form['indexes']) as $key => $id) {
    // To avoid silly format warnings.
    $key = $key;
    $rows[] = array(
      'data' => array(
        drupal_render($form['indexes'][$id]['index']),
        drupal_render($form['indexes'][$id]['value']),
      ),
    );
  }

  $output = theme('table', array('header' => $header, 'rows' => $rows));
  $output .= drupal_render($form['multispeak_description']);
  return $output;
}

/**
 * Generates a string representation of an array.
 *
 * This string format is suitable for edition in a textarea.
 *
 * @param array $values
 *   An array of values, where array keys are values and array values are
 *   labels.
 *
 * @return string
 *   The string representation of the $values array:
 *    - Values are separated by a carriage return.
 *    - Each value is in the format "name|value" or "value".
 */
function _multispeak_get_name_value_string(array $values) {
  $lines = array();
  foreach ($values as $name => $value) {
    // Convert data types.
    // @todo: #2251377: Json utility class serializes boolean values to incorrect data type.
    if (is_bool($value)) {
      $value = ($value) ? 'true' : 'false';
    }

    $lines[] = "$name|$value";
  }
  return implode("\n", $lines);
}

/**
 * Prepare form data types for Json conversion.
 *
 * @param array $values
 *   Array of name/value pairs.
 *
 * @return array
 *   Array of name/value pairs with casted data types.
 */
function _multispeak_convert_form_value_data_types(array $values) {

  foreach ($values as $name => $value) {
    // Convert data types.
    // @todo: #2251377: Json utility class serializes boolean values to incorrect data type.
    $match = drupal_strtolower($value);
    if ($match == 'true') {
      $value = TRUE;
    }
    elseif ($match == 'false') {
      $value = FALSE;
    }

    // Convert other known fields.
    // @todo: #2251343: Json utility class serializes numeric values to incorrect data type.
    switch ($name) {
      case 'sampleRate':
            // Float.
            settype($value, 'float');
        break;

      case 'siteSpeedSampleRate':
          case 'cookieExpires':
            // Integer.
            settype($value, 'integer');
        break;
    }

    $values[$name] = $value;
  }

  return $values;
}
