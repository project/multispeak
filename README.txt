
Module: Multispeak
Author: Gustavo Izquierdo <https://www.drupal.org/user/3248584>


Description
===========
MultiSpeak unlocks a full suite of communication options. Experience live video
and audio alongside a robust chat system, in one easy to use interface.

Requirements
============

* Multispeak (free) user account


Installation
============
Copy the 'multispeak' module directory in to your Drupal
sites/all/modules directory as usual.


Usage
=====
In the settings page enter your Multispeak company ID account number.

All pages will now have the required JavaScript added to the
HTML footer can confirm this by viewing the page source from
your browser.
