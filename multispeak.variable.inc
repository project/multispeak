<?php

/**
 * @file
 * Definition of variables for Variable API module.
 */

/**
 * Implements hook_variable_info().
 */
function multispeak_variable_info($options) {

  $variables['multispeak_account'] = array(
    'type' => 'string',
    'title' => t('Company ID', array(), $options),
    'default' => '',
    'description' => t('To get your company ID number, <a target="_blank" href="@register">register your site with Multispeak</a>, or if you already have registered your site, go to your Multispeak <a target="_blank" href="@admin">admin page</a> to get the ID under "Settings > Code".', array('@register' => 'https://multispeak.io/#get_started', '@admin' => url('https://multispeak.io/admin')), $options),
    'required' => TRUE,
    'group' => 'multispeak',
    'localize' => TRUE,
    'multidomain' => TRUE,
    'validate callback' => 'multispeak_validate_multispeak_account',
  );

  return $variables;
}

/**
 * Implements hook_variable_group_info().
 */
function multispeak_variable_group_info() {

  $groups['multispeak'] = array(
    'title' => t('Multispeak'),
    'description' => t('MultiSpeak widget unlocks a full suite of communication options for you and your customers.'),
    'access' => 'administer multispeak',
    'path' => array('admin/config/system/multispeak'),
  );

  return $groups;
}

/**
 * Validate Web Property ID variable.
 */
function multispeak_validate_multispeak_account($variable) {

  if (strlen($variable['value']) != 10) {
    return t('A valid Multispeak company id must have 10 digits.');
  }
}
